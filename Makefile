SHELL=bash
PROJ_DIR := $(shell pwd)


$(eval export $(shell sed -ne 's/ *#.*$//; /./ s/=.*$$// p' .env))

SERVICE_ADDR := 172.25.78.153
SERVICE_NAME := sql_sup
SERVICE_PASSWORD := 8FWrDX6nB9zCreE5

run: .env-export get_deps
	. .env-export && \
	iex --name $(SERVICE_NAME)@$(SERVICE_ADDR) \
	--cookie $(SERVICE_PASSWORD) \
	--erl '-kernel inet_dist_listen_min 9000' \
	--erl '-kernel inet_dist_listen_max 10000' \
	-S mix

get_deps:
	HEX_HTTP_CONCURRENCY=4 HEX_HTTP_TIMEOUT=120 mix deps.get

.env-export: .env
	sed -ne '/^export / {p;d}; /.*=/ s/^/export / p' .env > .env-export
