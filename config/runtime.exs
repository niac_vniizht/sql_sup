import Config

config :mnesia, :dir, [__DIR__, "..", "priv", "data"]
  |> Path.join()
  |> Path.expand()
  |> to_charlist()
