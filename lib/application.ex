defmodule SqlSup.Application do
  @moduledoc false
  use Supervisor
  @cluster_name :sql_sup_service
  @where_is_global (System.get_env("GLOBAL_ADDR") || "global@172.25.78.153") |> String.downcase() |> String.to_atom()

  def start_link(opts), do: Supervisor.start_link(__MODULE__, :ok, opts)

  def topologies do
    [
      scandinavian: [
        strategy: Cluster.Strategy.Epmd,
        config: [hosts: [@where_is_global]]
      ]
    ]
  end

  def init(:ok) do
    gnode = [global_node_addr: @where_is_global]

    children = [
      # TODO: сделать так, чтобы можно было вызывать MuspelheimLogger

      {Cluster.Supervisor, [topologies(), [name: @cluster_name]]},
      # SqlSup.MuspelheimApiServer,

      {Registry, keys: :unique, name: SqlSup.RepoRegistry},
      SqlSup.RepoSup,
      SqlSup.CleanerBot,
      SqlSup.WritterAgent,
      SqlSup.WritterFsm,
      SqlSup.Counter,
      # {SqlSup.GlobalGuardian.Server, {:global, make_name("sql_sup_test")}},
      {SqlSup.GlobalGuardian.Server, {:global, make_name("sql_sup")}},
      {SqlSup.GStore, gnode},

    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  ## """
  ##
  ##  Достает переменную окружения или подставляет имя
  ##
  ## """
  defp make_name(default) do
    "SERVER_NAME"
    |> System.get_env
    |> Kernel.||(default)
    |> String.downcase()
    |> String.to_atom()
  end
end
