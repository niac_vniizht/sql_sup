defmodule SqlSup.MuspelheimApiServer do
  @moduledoc """
  Модуль общения с сервисом логирования
  """
  use SqlSup.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :api,
    log_path: "priv/unhandled_logs",
    service_name: "muspelheim"

  def important_list, do: ~w(create_info_log create_warn_log create_debug_log create_error_log)a

  def create_info_log(from, msg),   do: cast({:create_log, :info, from, msg})
  def create_warn_log(from, msg),   do: cast({:create_log, :warn, from, msg})
  def create_debug_log(from, msg),  do: cast({:create_log, :debug, from, msg})
  def create_error_log(from, msg),  do: cast({:create_log, :error, from, msg})
  def list_muspelheim_logs(),       do: call(:list_logs)
end
