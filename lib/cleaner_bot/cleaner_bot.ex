defmodule SqlSup.CleanerBot do
  use GenServer

  @time 20 * 60_000

  def init(_args) do
    shedule()
    {:ok, %{}}
  end
  def start_link(args), do: GenServer.start_link(__MODULE__, args)

  def handle_info(:job, state) do
    shedule()
    job()
    {:noreply, state}
  end

  defp job, do: SqlSup.RepoSup.stop_useless()
  defp shedule, do: Process.send_after(self(), :job, @time)
end
