defmodule SqlSup.Counter do
  @moduledoc false
  use Agent
  @minute 60
  @hour @minute * 60
  @day @hour * 24
  @week @day * 7
  @divisor [@week, @day, @hour, @minute, 1]

  def start_link(_opts) do
    today = Date.utc_today() |> Date.to_string() |> String.to_atom()

    initial_value = %{
      :started => NaiveDateTime.local_now(),
      today => start_value()
    }

    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  def value do
    res = Agent.get(__MODULE__, & &1)
    %{started: started} = res
    now = NaiveDateTime.local_now()
    period = NaiveDateTime.diff(now, started)

    daily =
      Map.delete(res, :started)
      |> Enum.map(fn {day, day_data} ->
        period = NaiveDateTime.diff(now, started)

        day_data
        |> Map.put(:day, day)
        |> Map.put(:period, sec_to_str(period))
      end)

    %{
      data: daily,
      period: sec_to_str(period),
      started: started,
      now: now
    }
  end

  @spec increment(binary(), binary(), :income | :insert, integer, integer) :: :ok
  def increment(db_name, table_name, status, count_written, count_failed) do
    {key, failure_key} = get_key(db_name, table_name, status)
    today = Date.utc_today() |> Date.to_string() |> String.to_atom()
    Agent.update(__MODULE__, fn state ->
      state
      |> updater(today, key, count_written)
      |> updater(today, failure_key, count_failed)
    end)
  end


  @spec get_key(binary(), binary(), :income | :insert) ::
            {:__incoming_another_data_count, :__incoming_another_data_count_failure}
          | {:__incoming_jobs_data_count, :__incoming_jobs_data_count_failure}
          | {:__incoming_logs_data_count, :__incoming_logs_data_count_failure}
          | {:__incoming_raw_data_count, :__incoming_raw_data_count_failure}
          | {:__inserted_another_data_count, :__inserted_another_data_count_failure}
          | {:__inserted_jobs_data_count, :__inserted_jobs_data_count_failure}
          | {:__inserted_logs_data_count, :__inserted_logs_data_count_failure}
          | {:__inserted_raw_data_count, :__inserted_raw_data_count_failure}
  def get_key("simple_data", _, :income),   do: {:__incoming_raw_data_count, :__incoming_raw_data_count_failure}
  def get_key("simple_data", _, :insert),   do: {:__inserted_raw_data_count, :__inserted_raw_data_count_failure}
  def get_key("14", "askr_logs", :income),  do: {:__incoming_logs_data_count, :__incoming_logs_data_count_failure}
  def get_key("14", "askr_logs", :insert),  do: {:__inserted_logs_data_count, :__inserted_logs_data_count_failure}
  def get_key("14", _, :income),            do: {:__incoming_jobs_data_count, :__incoming_jobs_data_count_failure}
  def get_key("14", _, :insert),            do: {:__inserted_jobs_data_count, :__inserted_jobs_data_count_failure}
  def get_key(_, _, :income),               do: {:__incoming_another_data_count, :__incoming_another_data_count_failure}
  def get_key(_, _, :insert),               do: {:__inserted_another_data_count, :__inserted_another_data_count_failure}



  def updater(state, today, key, value) do
    curr_stat =
      state
      |> Map.get(today)
      |> case do
        nil ->
          start_value()

        obj ->
          if Map.has_key?(obj, key), do: Map.update(obj, key, 0, &(&1 + value)), else: obj
      end

    Map.put(state, today, curr_stat)

  end

  defp sec_to_str(sec) do
    {_, [s, m, h, d, w]} =
      Enum.reduce(@divisor, {sec, []}, fn divisor, {n, acc} ->
        {rem(n, divisor), [div(n, divisor) | acc]}
      end)

    ["#{w} недель", "#{d} дней", "#{h} часов", "#{m} минут", "#{s} секунд"]
    |> Enum.reject(fn str -> String.starts_with?(str, "0") end)
    |> Enum.join(", ")
  end

  defp start_value() do
    %{
      started: NaiveDateTime.local_now(),
      __incoming_jobs_data_count: 0,
      __incoming_raw_data_count: 0,
      __incoming_logs_data_count: 0,
      __incoming_another_data_count: 0,
      __inserted_jobs_data_count: 0,
      __inserted_raw_data_count: 0,
      __inserted_logs_data_count: 0,
      __inserted_another_data_count: 0,
      __incoming_jobs_data_count_failure: 0,
      __incoming_raw_data_count_failure: 0,
      __incoming_logs_data_count_failure: 0,
      __incoming_another_data_count_failure: 0,
      __inserted_jobs_data_count_failure: 0,
      __inserted_raw_data_count_failure: 0,
      __inserted_logs_data_count_failure: 0,
      __inserted_another_data_count_failure: 0,
    }
  end
end
