defmodule SqlSup.GlobalGuardian.Condition do
  @moduledoc false
  require Logger


  def request({:run_query_raw, raw_query, auth}),                                                                      do: SqlSup.DynnoRepo.run_query(raw_query, auth)
  # def request({:insert_chunked, repo_params, list, table_name, chunk}), do:             SqlSup.RepoSup.insert_chunked(repo_params, list, table_name, chunk)
  # def request({:insert_chunked, repo_params, list, table_name}), do:                    SqlSup.RepoSup.insert_chunked(repo_params, list, table_name)
  def request({:insert_chunked, repo_params, list, table_name, _chunk}) when is_tuple(list) and is_tuple(repo_params), do: SqlSup.WritterFsm.handle(reparse_unhandled(repo_params), "#{table_name}", reparse_packs(list)) |> elem(1)
  def request({:insert_chunked, repo_params, list, table_name}) when is_tuple(list) and is_tuple(repo_params),         do: SqlSup.WritterFsm.handle(reparse_unhandled(repo_params), "#{table_name}", reparse_packs(list)) |> elem(1)
  def request({:insert_chunked, repo_params, list, table_name, _chunk}) when is_list(list),                            do: SqlSup.WritterFsm.handle(repo_params, table_name, list) |> elem(1)
  def request({:insert_chunked, repo_params, list, table_name}) when is_list(list),                                    do: SqlSup.WritterFsm.handle(repo_params, table_name, list) |> elem(1)
  def request({:insert_chunked, repo_params, list, table_name, _chunk}) ,                                    do: throw([repo_params, list, table_name])
  def request({:insert_chunked, repo_params, list, table_name}) ,                                    do: throw([repo_params, list, table_name])

  def request({:insert_chunked_sync, repo_params, list, table_name, _chunk}) when is_tuple(list) and is_tuple(repo_params), do: SqlSup.RepoSup.insert_chunked(reparse_unhandled(repo_params), reparse_packs(list), "#{table_name}")
  def request({:insert_chunked_sync, repo_params, list, table_name}) when is_tuple(list) and is_tuple(repo_params),         do: SqlSup.RepoSup.insert_chunked(reparse_unhandled(repo_params), reparse_packs(list), "#{table_name}")
  def request({:insert_chunked_sync, repo_params, list, table_name, _chunk}) when is_list(list),                            do: SqlSup.RepoSup.insert_chunked(repo_params, list, table_name)
  def request({:insert_chunked_sync, repo_params, list, table_name}) when is_list(list),                                    do: SqlSup.RepoSup.insert_chunked(repo_params, list, table_name)
  def request({:insert_chunked_sync, repo_params, list, table_name, _chunk}) ,                                              do: throw([repo_params, list, table_name])
  def request({:insert_chunked_sync, repo_params, list, table_name}) ,                                                      do: throw([repo_params, list, table_name])


  def request({:run_query, repo_params, callback}),                                                                    do: SqlSup.RepoSup.run_query(repo_params, callback)
  def request({:run_query, repo_params, callback, params}),                                                            do: SqlSup.RepoSup.run_query(repo_params, callback, params)
  def request({:run_query_safely, repo_params, callback}),                                                             do: SqlSup.RepoSup.run_query_safely(repo_params, callback)
  def request({:run_query_safely, repo_params, callback, params}),                                                     do: SqlSup.RepoSup.run_query_safely(repo_params, callback, params)
  def request(:counter_value),                                                                                         do: SqlSup.Counter.value()
  def request({:select_logs, auth, table, dt_start, dt_end}),                                                          do: SqlSup.RecalcHelper.select_raw_data(auth, table, dt_start, dt_end)
  def request({:select_logs_122, auth, table, imei, pack_number, dt_start, dt_end}),                                   do: SqlSup.RecalcHelper.select_logs_122(auth, table, imei, pack_number, dt_start, dt_end)
  def request({:is_table_exist?, repo_params, table_name}),                                                            do: SqlSup.RepoSup.is_table_exist?(repo_params, table_name)
  def request({:get_raw_data, repo_params, table_name, params}),                                                       do: SqlSup.RecalcHelper.get_raw_data(repo_params, table_name, params)
  def request({:get_job_data, repo_params, table_name, params}),                                                       do: SqlSup.RecalcHelper.get_job_data(repo_params, table_name, params)
  def request({:delete_raw_data, repo_params, table_name, params}),                                                    do: SqlSup.RecalcHelper.delete_raw_data(repo_params, table_name, params)
  def request({:delete_job_data, repo_params, table_name, params}),                                                    do: SqlSup.RecalcHelper.delete_job_data(repo_params, table_name, params)

  def request(other) do
      Logger.warn(%{reason: "no-condition", params: other, request_type: "handler"})
      {:error, "no-condition"}
  end

  def super_condition(param, state), do: {:reply, request(param), state}

  def reparse_packs(tuple) when is_tuple(tuple), do: reparse_packs(Tuple.to_list(tuple))
  def reparse_packs(list_of_packs) do
    list_of_packs
    |> Stream.map(& reparse_unhandled(&1))
    |> Stream.map(& Enum.into(&1, %{}))
    |> Enum.to_list()
  end

  def reparse_unhandled(tuple) when is_tuple(tuple), do: reparse_unhandled(Tuple.to_list(tuple))
  def reparse_unhandled(keywd_list) do
    Enum.reduce(keywd_list, [], fn
      {:is_valid, val}, acc  -> [{:is_valid, to_string(val)} | acc]
      {key, val}, acc when is_atom(val)  -> [{key, to_string(val)} | acc]
      {key, val}, acc when is_tuple(val) -> [{key, Tuple.to_list(val)} | acc]
      {key, val}, acc -> [{key, val} | acc]
    end)
    |> Enum.reverse()
  end
end
