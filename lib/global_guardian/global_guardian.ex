defmodule SqlSup.GlobalGuardian.Server do
  @moduledoc false
  use GenServer

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(opts), do: GenServer.start_link(__MODULE__, [], name: opts)

  @spec init(any) :: {:ok, any}
  def init(stack), do: {:ok, stack}

  def handle_call({:factorial, num}, _from, state), do: {:reply, with_bench(&factorial/1, num), state}
  def handle_call(param, _from, state), do: SqlSup.GlobalGuardian.Condition.super_condition(param, state)

  defp factorial(n) when n > 0, do: n * factorial(n - 1)
  defp factorial(0), do: 1
  defp factorial(n) when n < 0, do: {:error, :negative_number}

  defp with_bench(func, params) when is_function(func) do
    IO.inspect("started calculations with #{inspect(params)}")
    st = System.monotonic_time()
    result = func.(params)
    finish = System.monotonic_time()
    IO.inspect("finished calculations with result: #{result}")
    IO.inspect("operation time:  #{(finish - st) / 1_000_000} ms")
    result
  end
end
