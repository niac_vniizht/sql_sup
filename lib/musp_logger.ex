defmodule SqlSup.MuspelheimLogger do
  @behaviour :gen_event

  alias SqlSup.MuspelheimApiServer
  def init(_args) do
    {:ok, %{}}
  end

  def handle_call({:configure, _options}, state) do
    {:ok, :ok, state}
  end

  def handle_event({level, _gl, {Logger, msg, _, metadata}}, state) do
    Process.whereis(:muspelheim_api_server)
    |> is_nil()
    |> Kernel.!()
    |> handle_log(level, msg, metadata)

    {:ok, state}
  end

  def handle_event(:flush, state) do
    {:ok, state}
  end

  defp service_name(metadata) do
    case metadata[:application] do
      nil -> "sql_sup"
      app -> "sql_sup: #{app}"
    end
  end

  defp handle_log(true, level, msg, metadata) do
    case level do
      :error -> MuspelheimApiServer.create_error_log(service_name(metadata), msg)
      :warn -> MuspelheimApiServer.create_warn_log(service_name(metadata), msg)
      :debug -> MuspelheimApiServer.create_debug_log(service_name(metadata), msg)
      _ -> MuspelheimApiServer.create_info_log(service_name(metadata), msg)
    end
  end

  defp handle_log(false, level, msg, metadata) do
    IO.inspect([level, msg, metadata])
  end
end
