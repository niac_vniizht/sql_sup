defmodule SqlSup.DynnoRepo do
  use Ecto.Repo, otp_app: :server, adapter: Ecto.Adapters.Postgres

  def run_query(raw_query, auth) when is_binary(raw_query) do
    __MODULE__.with_dynamic_repo(auth, fn -> __MODULE__.query(raw_query, [], timeout: :timer.minutes(5)) end)
    |> make_maps
  end

  def with_dynamic_repo(credentials, callback) do
    default_dynamic_repo = get_dynamic_repo()
    start_opts = [name: nil, pool_size: 1, log: false, types: SqlSup.PostgresTypes] ++ credentials
    {:ok, repo} = __MODULE__.start_link(start_opts)

    try do
      __MODULE__.put_dynamic_repo(repo)
      callback.()
    after
      __MODULE__.put_dynamic_repo(default_dynamic_repo)
      Supervisor.stop(repo)
    end
  end

  def make_maps({:ok, map}),  do: make_maps(map)
  def make_maps(%{columns: cols, rows: rows}) when not is_nil(cols) and not is_nil(rows)  do
    at_cols = cols |> Enum.map(&String.to_atom/1)
    rows
    |> Enum.map(&Enum.zip(at_cols, &1) |> Enum.into(%{}))
  end
  def make_maps(_), do: :ok
end
