defmodule SqlSup.RecalcHelper do
  @moduledoc false
  import Ecto.Query, warn: false

  def select_raw_data(auth, table, dt_start, dt_end) do
    callback = fn ->
      from(table)
      |> where([t], t.dt >= ^dt_start)
      |> where([t], t.dt <= ^dt_end)
      |> select(
        [t],
        %{
          id: t.id,
          auth: t.pm_params["auth"],
          data_string: t.data_string,
          data: t.data,
          device_id: t.device_id,
          dt: t.dt,
          imei: t.imei,
          machine_id: t.machine_id
        }
      )
      |> order_by(asc: :id)
      |> SqlSup.Repo.all(timeout: :timer.minutes(30))
    end

    SqlSup.RepoSup.run_query(auth, callback)
  end

  def select_logs_122(auth, table, imei, pack_number, dt_start, dt_finish) do
    callback = fn ->
      from(table)
      |> where([t], t.pack_dt >= ^dt_start)
      |> where([t], t.pack_dt <= ^dt_finish)
      |> where([t], t.imei == ^imei)
      |> where([t], t.pack_number == ^pack_number)
      |> select(
        [t],
        %{
          data: t.data,
          key: t.key,
        }
      )
      |> SqlSup.Repo.all(timeout: :timer.minutes(1))
    end

    SqlSup.RepoSup.run_query(auth, callback)
  end

  # Удаление данных
  def delete_raw_data(repo_params, table_name, params) do
    case raw_data_query(table_name, params) do
      {:error, error} ->
        {:error, error}

      query ->
        SqlSup.RepoSup.run_query(repo_params, fn ->
          SqlSup.Repo.delete_all(query, timeout: :timer.minutes(30))
        end)
    end
  end

  def delete_job_data(
        [
          hostname: "172.25.78.167",
          username: _username,
          password: _password,
          database: "14",
          port: 5437
        ] = repo_params,
        table_name,
        params
      ) do
    case job_data_query(table_name, params) do
      {:error, error} ->
        {:error, error}

      query ->
        SqlSup.RepoSup.run_query(repo_params, fn ->
          SqlSup.Repo.delete_all(query, timeout: :timer.minutes(30))
        end)
    end
  end

  def delete_job_data(
        [
          hostname: hostname,
          username: _,
          password: _,
          database: database,
          port: port
        ],
        table_name,
        _params
    ) do
    {:error, "table cannot be recalculated (hostname: #{hostname}, database: #{database}, port: #{port}, table: #{table_name})"}
  end


  # Получение данных
  def get_raw_data(repo_params, table_name, params) do
    case raw_data_query(table_name, params) do
      {:error, error} ->  {:error, error}
      query ->
        SqlSup.RepoSup.run_query(repo_params, fn ->
          query
          |> select([t], %{
            id: t.id,
            dt: t.dt,
            imei: t.imei
          })
          |> SqlSup.Repo.all(timeout: :timer.minutes(30))
        end)
    end
  end

  def get_job_data(
        [
          hostname: "172.25.78.167",
          username: _username,
          password: _password,
          database: "14",
          port: 5437
        ] = repo_params,
        table_name,
        params
      ) do
    case job_data_query(table_name, params) do
      {:error, error} ->
        {:error, error}

      query ->
        SqlSup.RepoSup.run_query(repo_params, fn ->
          query
          |> select([t], %{
            id: t.id,
            dt: t.dt,
            imei: t.imei
          })
          |> SqlSup.Repo.all(timeout: :timer.minutes(30))
        end)
    end
  end

  def get_job_data(
        [
          hostname: hostname,
          username: _,
          password: _,
          database: database,
          port: port
        ],
        table_name,
    _params) do
    {:error, "table cannot be recalculated (hostname: #{hostname}, database: #{database}, port: #{port}, table: #{table_name})"}
  end

  # Запросы данных
  def raw_data_query(_table_name, params) when params == %{}, do: {:error, "no parameters passed"}
  def raw_data_query(table_name, params),
    do: Enum.reduce(params, from(t in table_name), &upd_raw_data_query(&1, &2))

  # def upd_raw_data_query({:pack_number, pack_number}, rows)         when is_binary(pack_number),      do: from(r in rows, where: r.pack_number == ^pack_number)
  # def upd_raw_data_query({:pack_length, pack_length}, rows)         when is_integer(pack_length),     do: from(r in rows, where: fragment("array_length(regexp_split_to_array(?, ','), 1)", r.data_string) == ^pack_length)
  def upd_raw_data_query({:imei, imei}, rows) when is_integer(imei),           do: from(r in rows, where: r.imei == ^imei)
  def upd_raw_data_query({:dt_start, %NaiveDateTime{} = dt_start}, rows),      do: from(r in rows, where: r.dt >= ^dt_start)
  def upd_raw_data_query({:dt_end, %NaiveDateTime{} = dt_end}, rows),          do: from(r in rows, where: r.dt <= ^dt_end)
  def upd_raw_data_query(par, _rows), do: {:error, "wrong parametr #{inspect(par)}"}

  def job_data_query(_table_name, params) when params == %{}, do: {:error, "no parameters passed"}

  def job_data_query(table_name, params),  do: Enum.reduce(params, from(t in table_name), &upd_job_data_query(&1, &2))

  def upd_job_data_query({:imei, imei}, rows) when is_integer(imei),        do: from(r in rows, where: fragment("imei::text") == ^to_string(imei))
  def upd_job_data_query({:dt_start, %NaiveDateTime{} = dt_start}, rows),   do: from(r in rows, where: r.dt >= ^dt_start)
  def upd_job_data_query({:dt_end, %NaiveDateTime{} = dt_end}, rows),       do: from(r in rows, where: r.dt <= ^dt_end)
  def upd_job_data_query(par, _rows), do: {:error, "wrong parametr #{inspect(par)}"}

  def get_repo_params(:simple_data) do
    [
      hostname: "172.25.78.167",
      username: "db_admin",
      password: "8FWrDX6nB9zCreE5",
      database: "simple_data",
      port: 5437
    ]
  end

  def get_repo_params(:"14") do
    [
      hostname: "172.25.78.167",
      username: "db_admin",
      password: "8FWrDX6nB9zCreE5",
      database: "14",
      port: 5437
    ]
  end
end
