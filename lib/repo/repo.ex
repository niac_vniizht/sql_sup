defmodule SqlSup.Repo do
  use Ecto.Repo, otp_app: :sql_sup, adapter: Ecto.Adapters.Postgres
end
