defmodule SqlSup.RepoSup do
  @moduledoc """
  Супервизор базы данных
  """
  use DynamicSupervisor
  require Logger
  require Decimal
  @supervisor_name :repo_supervisor
  alias SqlSup.{RepoRegistry, RepoWorker, RequestBuilder}

  @stop_time 2 * 60 * 60 * 1000

  #
  # Клиентские функции
  #

  @spec create(any) :: {:error, :already_exists} | {:ok, pid}
  def create(repo_params) do
    case find(repo_params) do
      {:ok, _pid} -> {:error, :already_exists}
      {:error, :unexisting_repo} -> {:ok, _pid} = start(repo_params)
    end
  end

  def stop_useless() do
    list_repos()
    |> Enum.each(fn pid ->
      now = NaiveDateTime.local_now()
      dt = RepoWorker.get_dt(pid)

      if NaiveDateTime.diff(now, dt) > @stop_time do
        DynamicSupervisor.terminate_child(:repo_supervisor, pid)
      end
    end)

    :ok
  end

  def run_query_safely(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback) when is_list(repo_params) and is_binary(callback) do
    {:ok, pid} = find_or_create(repo_params)
    RepoWorker.run_query_safely(pid, raw_func: callback)
  end
  def run_query_safely(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback, params) when is_list(repo_params) and is_binary(callback) do
    {:ok, pid} = find_or_create(repo_params)
    RepoWorker.run_query_safely(pid, params, raw_func: callback)
  end


  def run_query(:no_gserver, repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback) do
    options = [log: false, types: SqlSup.PostgresTypes] ++ repo_params
    {:ok, pid} = Postgrex.start_link(options)

    %{columns: cols, rows: rows} = Postgrex.query!(pid, callback, [], [timeout: :timer.minutes(10)])
    GenServer.stop(pid)

    cols = Enum.map(cols, & String.to_atom(&1))
    Enum.map(rows, & Enum.into(Enum.zip(cols, &1), %{}))
  end

  def run_query(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback, params) when is_list(repo_params) and is_binary(callback) do
    {:ok, pid} = find_or_create(repo_params)
    # |> IO.inspect
    RepoWorker.run_query(pid, params, raw_func: callback)
  end

  def run_query(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback, params) when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)
    RepoWorker.run_query(pid, params, callback: callback)
  end

  def run_query(pid, callback, params) when is_binary(callback),  do: RepoWorker.run_query(pid, params, raw_func: callback)
  def run_query(pid, callback, params), do: RepoWorker.run_query(pid, params, callback: callback)

  def run_query(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback) when is_list(repo_params) and is_binary(callback) do
    {:ok, pid} = find_or_create(repo_params)
    # |> IO.inspect
    RepoWorker.run_query(pid, raw_func: callback)
  end

  def run_query(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt], callback) when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)
    RepoWorker.run_query(pid, callback: callback)
  end
  def run_query(pid, callback) when is_binary(callback),  do: RepoWorker.run_query(pid, raw_func: callback)
  def run_query(pid, callback), do: RepoWorker.run_query(pid, callback: callback)

  def run_query(_pms), do: {:error, :invalid_options}

  def list_repos do
    RepoRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(&elem(&1, 1))
  end

  def insert_chunked(repo_params, list, table_name, chunk \\ 100)

  def insert_chunked(repo_params, list, table_name, chunk) when is_list(repo_params) do
    hostname = repo_params[:hostname]
    mode = if(hostname == "10.110.197.42", do: :normal, else: :transactional)

    list =
      cond do
        hostname == "10.110.197.42" ->
          list
          |> Enum.map(fn
            %{id_repl: 42} = item -> Map.put(item, :id_repl, nil)
            %{id_repl: 14} -> nil
            item -> item
          end)
          |> Enum.reject(&is_nil/1)
        hostname == "172.25.78.14" ->
          list
          |> Enum.map(fn
            %{id_repl: 14} = item -> Map.put(item, :id_repl, nil)
            item -> item
          end)
        true ->
          list
      end

    with {:ok, pid} <- find_or_create(repo_params),
        {:inserted, count} <- insert_chunked(mode, pid, list, table_name, chunk) do
      {:inserted, count}
    else
      e -> e
    end
  end

  def insert_chunked(:transactional, pid, list, table_name, chunk) do
    max_struct = RequestBuilder.max_struct(list)
    nilified_struct = max_struct |> Enum.map(fn {k, _v} -> {k, nil} end) |> Enum.into(%{})

    case update_table_cols(pid, max_struct, table_name) do
      :ok ->
        {:ok, data} =
          list
          |> Enum.map(&Map.merge(nilified_struct, &1))
          |> update_to_table(pid, table_name)
          |> Enum.chunk_every(chunk, chunk, [])
          |> Enum.with_index()
          |> Enum.reduce(Ecto.Multi.new(), fn {items, index}, acc ->
            Ecto.Multi.insert_all(acc, :"insert_#{index}", table_name, items)
          end)
          |> (fn reductions -> run_query(pid, fn -> SqlSup.Repo.transaction(reductions, timeout: :timer.minutes(5)) end) end).()



        {inserted, rejected} =
          data
          |> Enum.reduce({0, 0}, fn {_k, {intd, rejtd}}, {acc_ins, acc_rej} ->
            {
              acc_ins + (intd || 0),
              acc_rej + (rejtd || 0)
            }
          end)

        {:inserted, inserted, rejected}

      error ->
        error
    end
  rescue
    e -> e

  end

  def insert_chunked(:normal, pid, list, table_name, chunk) do
    max_struct = RequestBuilder.max_struct(list)
    nilified_struct = max_struct |> Enum.map(fn {k, _v} -> {k, nil} end) |> Enum.into(%{})

    case update_table_cols(pid, max_struct, table_name) do
      :ok ->
        data =
          list
          |> Enum.map(&Map.merge(nilified_struct, &1))
          |> update_to_table(pid, table_name)
          |> Enum.chunk_every(chunk, chunk, [])
          |> Enum.with_index()
          |> Enum.reduce([], fn {items, index}, acc ->
            try do
              run_query(pid, fn -> SqlSup.Repo.insert_all(table_name, items) end)
              [{:"#{index}", {length(items), 0}} | acc]
            rescue
              _e ->
                [{:"#{index}", {0, length(items)}} | acc]
            end
          end)

        {inserted, rejected} =
          data
          |> Enum.reduce({0, 0}, fn {_k, {intd, rejtd}}, {acc_ins, acc_rej} ->
            {
              acc_ins + (intd || 0),
              acc_rej + (rejtd || 0)
            }
          end)

        {:inserted, inserted, rejected}

      error ->
        error
    end
  rescue
    e ->
      e
  end

  def update_to_table(list, pid, table_name) do
    repo_table_struct =
      get_full_table_struct(pid, table_name)
      |> RequestBuilder.prepare_struct_to_diff()
      |> Enum.map(fn {key, type} -> {String.to_atom(key), type} end)

    list
    |> Enum.reduce([], fn structure, new_list ->
      Enum.reduce(repo_table_struct, %{}, fn
        _, :error -> :error
        {key, type}, acc ->
          val = Map.get(structure, key) |> get_val(type)
          if val == :error do
            :error
          else
            Map.put(acc, key, val)
          end
      end)
      |> case do
        :error -> new_list
        map -> [map | new_list]
      end
      |> Enum.reverse()
    end)
  end


  @spec get_val(any, any) :: any
  def get_val("", _), do: nil

  def get_val(%Geo.Point{} = val, _), do: val
  def get_val(%Geo.LineString{} = val, _), do: val

  def get_val(%{srid: 4326, coordinates: [gps_lon, gps_lat], properties: _props}, _) do
    cond do
      is_float(gps_lon) and is_float(gps_lat) ->
        %Geo.Point{coordinates: {gps_lon, gps_lat}, srid: 4326}

      true ->
        throw("type cast declared only for geo points")
    end
  end

  # TODO: доделать и протестировать нормально
  def get_val(val, "numeric " <> params) when is_float(val) do
    [precision, scale] = String.split(params, ["(", ",", ")", " "], trim: true)

    case {Integer.parse(scale), Integer.parse(precision)} do
      {{int_scale, _}, {int_precision, _}} ->
        new_val = Float.round(val, int_scale)
        if new_val < :math.pow(10, int_precision - int_scale + 1) do
          new_val
        else
          throw("cannot cast #{val} to numeric " <> params)
        end
      {_, {int_precision, _}} ->
        if val < :math.pow(10, int_precision + 1) do
          val
        else
          throw("cannot cast #{val} to numeric " <> params)
        end
      {_, _} -> val
    end
  end


  def get_val(val, "decimal") when is_integer(val), do: val / 1
  def get_val(val, "bigint") when is_float(val), do: round(val)
  def get_val(val, "bigint") when is_binary(val), do: Integer.parse(val) |> elem(0)
  def get_val(val, "varchar(255)") when is_float(val), do: Float.to_string(val)
  def get_val(val, "varchar(255)") when is_integer(val), do: Integer.to_string(val)
  def get_val(val, "varchar(255)") when is_map(val), do: Poison.encode!(val)
  def get_val(val, "varchar(255)"), do: inspect(val)

  def get_val(val, type) when type in ["timestamp", "timestamp without time zone"] and is_binary(val),
    do: NaiveDateTime.from_iso8601!(val) |> DateTime.from_naive!("Etc/UTC") |> get_val(type)

  def get_val(val, "date") when is_binary(val), do: Date.from_iso8601!(val) |> get_val("date")

  def get_val(%Date{} = val, type) when type in ["timestamp", "timestamp without time zone"] do
    val = Timex.to_datetime(val)
    res = if Timex.is_valid?(val), do: val, else: :error
    if res == :error, do: IO.inspect({val, type})
    res
  end

  def get_val(val, type) when type in ["timestamp", "timestamp without time zone", "date"] and not is_nil(val) do
    res = if Timex.is_valid?(val), do: val, else: :error
    if res == :error, do: IO.inspect({val, type})
    res
  end

  def get_val(val, "boolean") when is_binary(val) do
    case val do
      "true" ->
        true

      "false" ->
        false

      _ ->
        throw("cannot cast #{val} to boolean")
    end
  end

  def get_val(val, "ARRAY") when is_list(val) do
    res = Enum.map(val, fn el ->
      res = get_val(el, "bigint")
      if res >= -2147483648 and res <= 2147483647 do
        res
      else
        :error
      end
    end)
    if :error in res do
      :error
    else
      res
    end
  end

  def get_val(val, _) when is_float(val), do: Float.round(val, 6)
  def get_val(val, _) when Decimal.is_decimal(val), do: Float.round(Decimal.to_float(val), 6)
  def get_val(val, _), do: val

  def update_table_cols(
        repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt],
        struct,
        table_name
      )
      when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)
    update_table_cols(pid, struct, table_name)
  end

  # TODO: можно будет переписать
  # def update_table_cols(pid, struct, table_name) do
  #   if is_table_exist?(pid, table_name) do
  #     repo_table_struct = get_table_struct(pid, table_name) |> RequestBuilder.prepare_struct_to_diff |> IO.inspect(label: :repo_table_struct)
  #     data_struct = struct |> RequestBuilder.fetch_structure() |> RequestBuilder.prepare_struct_to_diff
  #     diff = data_struct -- repo_table_struct
  #     cond do
  #       diff == [] -> :ok
  #       diff != [] && !RequestBuilder.only_type_diff(data_struct, repo_table_struct) ->
  #         try do
  #           update_func = RequestBuilder.create_add_table_column_query(diff, table_name)
  #           RepoWorker.run_query(pid, raw_func: update_func)
  #           :ok
  #         rescue
  #           _e ->
  #             {:error, :field_exists}
  #         end
  #       true -> {:error, :cant_rewrite_fields}
  #     end
  #   else
  #     create_if_not_exist(pid, struct, table_name)
  #     update_table_cols(pid, struct, table_name)
  #   end
  # end

  def update_table_cols(pid, struct, table_name) do
    # IO.inspect({pid, table_name})
    if is_table_exist?(pid, table_name) do
      repo_table_struct =
        get_full_table_struct(pid, table_name) |> RequestBuilder.prepare_struct_to_diff()

      data_struct =
        struct |> RequestBuilder.fetch_structure() |> RequestBuilder.prepare_struct_to_diff()

      case data_struct -- repo_table_struct do
        [] ->
          :ok

        diff ->
          keys = RequestBuilder.get_keys(diff)

          errors =
            Enum.reduce(keys, [], fn key, acc ->

              case {Enum.find(repo_table_struct, &(elem(&1, 0) == key)),
                    Enum.find(data_struct, &(elem(&1, 0) == key))} do
                {nil, d_el} ->
                  update_func = RequestBuilder.create_add_table_column_query([d_el], table_name)
                  RepoWorker.run_query(pid, raw_func: update_func)
                  acc

                {t_el, d_el} ->
                  if RequestBuilder.irreducible_types(elem(t_el, 1), elem(d_el, 1)) do
                    [
                      "Неудачная попытка добавить поле #{elem(d_el, 0)}. Передан тип #{elem(d_el, 1)}, ожидается тип #{elem(t_el, 1)}"
                      | acc
                    ]
                  else
                    acc
                  end
              end
            end)

          if errors != [], do: errors, else: :ok
      end
    else
      # [
      #   "Таблица #{table_name} не существует."
      # ]
      create_if_not_exist(pid, struct, table_name)
      update_table_cols(pid, struct, table_name)
    end
  end

  def create_if_not_exist(
        repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt],
        struct,
        table_name
      )
      when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)
    create_if_not_exist(pid, struct, table_name)
  end

  def create_if_not_exist(pid, struct, table_name) do
    if is_table_exist?(pid, table_name) do
      :ok
    else
      create_func =
        struct
        |> RequestBuilder.fetch_structure()
        |> RequestBuilder.create_table_builder(table_name)

      RepoWorker.run_query(pid, raw_func: create_func)
      :ok
    end
  end

  def get_table_struct(
        repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt],
        table_name
      )
      when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)

    RepoWorker.run_query(pid,
      raw_func:
        "SELECT column_name, data_type from information_schema.columns where table_name = '#{table_name}';"
    )
  end

  def get_table_struct(pid, table_name) when is_pid(pid) do
    RepoWorker.run_query(pid,
      raw_func:
        "SELECT column_name, data_type from information_schema.columns where table_name = '#{table_name}';"
    )
  end

  def get_full_table_struct(
        repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt],
        table_name
      )
      when is_list(repo_params) do
    {:ok, pid} = find_or_create(repo_params)

    RepoWorker.run_query(pid,
      raw_func: "SELECT * from information_schema.columns where table_name = '#{table_name}';"
    )
  end

  def get_full_table_struct(pid, table_name) when is_pid(pid) do
    RepoWorker.run_query(pid,
      raw_func: "SELECT * from information_schema.columns where table_name = '#{table_name}';"
    )
  end

  def is_table_exist?(
        repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt],
        table_name
      )
      when is_list(repo_params) do
    # IO.inspect(repo_params)
    {:ok, pid} = find_or_create(repo_params)
    is_table_exist?(pid, table_name)
  rescue
    _e ->
      Logger.error(%{
        message: "Возникла ошибка при записи данных в таблицу (#{table_name}, #{inspect(repo_params)})"
      })
      {:error, "Возникла ошибка при записи данных в таблицу (#{table_name}, #{inspect(repo_params)})"}
  end

  def is_table_exist?(pid, table_name) when is_pid(pid) do
    RepoWorker.run_query(pid, raw_func: "SELECT to_regclass('#{table_name}');")
    |> List.first()
    |> Map.get(:to_regclass)
    |> is_nil()
    |> Kernel.!()
  end

  #
  # Серверные функции
  #

  def start_link(_opts), do: DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  def init(_opts), do: DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])

  #
  # Приватные функции
  #

  defp start(repo_params = [hostname: _h, username: _u, password: _p, database: _d, port: _prt]) do
    name = {:via, Registry, {RepoRegistry, repo_params}}

    DynamicSupervisor.start_child(@supervisor_name, {RepoWorker, name})
  end

  defp start(_any), do: {:error, :invalid_params}

  defp find_or_create(repo_params) do
    start_valid(
      repo_params,
      fn ->
        case Registry.lookup(RepoRegistry, repo_params) do
          [] -> {:ok, _pid} = start(repo_params)
          [{nil, _any}] -> start(repo_params)
          [{pid, _any}] -> {:ok, pid}
        end
      end
    )
  end

  defp find(repo_params) do
    case Registry.lookup(RepoRegistry, repo_params) do
      [] -> {:error, :unexisting_repo}
      [{nil, _any}] -> start(repo_params)
      [{pid, _any}] -> {:ok, pid}
    end
  end

  #
  # start_valid(repo_params, func) - запустит вызов функции валидации и подставит вторым параметром результат
  # start_valid(repo_params, validution_result, func)
  # repo params - [hostname: _h, username: _u, password: _p, database: _d, port: _prt]
  # validution_result - boolean value (результат валидации)
  # func/0 - функция, которую нужно использовать в случае валидности
  #

  defp start_valid(repo_params, func), do: start_valid(repo_params, validate_repo_params(:auth, repo_params), func)
  defp start_valid(repo_params, false, _func), do: {:error, "invalid repo params(#{inspect(repo_params)})"}
  defp start_valid(_repo_params, true, func), do: func.()

  defp validate_repo_params(:auth, repo_params) do
    [hostname: nil, username: nil, password: nil, database: nil, port: nil]
    |> Keyword.merge(repo_params)
    |> Enum.reduce(false, fn
      _, true -> true
      {:hostname, hname}, false when is_binary(hname) -> !String.match?(hname, ~r/^\d+.\d+.\d+.\d+$/)
      {:username, str}, false when is_binary(str) -> false
      {:password, str}, false when is_binary(str) -> false
      {:database, str}, false when is_binary(str) -> false
      {:port, port}, false when is_integer(port)  -> false
      _, false -> true
    end)
    |> Kernel.!()
  end
end
