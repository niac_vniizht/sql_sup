defmodule SqlSup.RepoWorker do
  @moduledoc """
  Описывает дочернии процессы для работы с базами данных
  """
  use GenServer
  @pool 20
  defstruct repo: nil, params: [], name: nil, dt: nil

  #
  # Клиентские функции
  #
  def run_query_safely(pid, params \\ [], [raw_func: raw_funcs]) do
    GenServer.call(pid, {:run_query, fn -> SqlSup.Repo.query(raw_funcs, params, timeout: :timer.minutes(10)) end}, :infinity) |> SqlSup.RequestBuilder.make_maps()
  end

  def run_query(pi, params \\ [], tuple)
  def run_query(nil, params, tuple), do: throw(inspect([tuple, params]))
  def run_query(pid, params, [raw_func: raw_funcs]) do
    raw_funcs =
      raw_funcs
      |> String.replace("\n", " ")
    GenServer.call(pid, {:run_query, fn -> SqlSup.Repo.query!(raw_funcs, params, timeout: :timer.minutes(10)) end}, :infinity) |> SqlSup.RequestBuilder.make_maps()
  end

  def run_query(pid, _params, [callback: callback]) do
    GenServer.call(pid, {:run_query, callback}, :infinity)
  end



  def get_dt(pid) do
    GenServer.call(pid, :get_dt)
  end

  #
  # Серверные функции
  #

  def start_link(name), do: create(name)
  def init(state) do
    Process.flag(:trap_exit, true)
    {:ok, state}
  end

  def create(name = {:via , Registry, {_registry_name, params}}) do
    options = [name: nil, pool_size: @pool, log: false, types: SqlSup.PostgresTypes] ++ params

    {:ok, repo} = SqlSup.Repo.start_link(options)

    GenServer.start_link(__MODULE__, %__MODULE__{repo: repo, params: options, dt: NaiveDateTime.local_now()}, name: name)
  end
  def create(_any), do: {:error, :invalid_params}

  def handle_call(:get_dt, _from, state = %__MODULE__{dt: dt}) do
    {:reply, dt, state}
  end

  def handle_call({:run_query, {:for_eval, callback, params}}, _from, state = %__MODULE__{repo: repo}) do
    SqlSup.Repo.put_dynamic_repo(repo)
    callback = Code.eval_string(callback, params) |> elem(0)
    res =
      try do
        callback.()
      rescue
        _e ->
          :timer.sleep(2000)
          callback.()
      catch
        _ ->
          :timer.sleep(2000)
          callback.()
        _, _ ->
          :timer.sleep(2000)
          callback.()
      end
    new_state = Map.put(state, :dt, NaiveDateTime.local_now())
    {:reply, res, new_state}
  end

  def handle_call({:run_query, callback}, _from, state = %__MODULE__{repo: repo}) do
    SqlSup.Repo.put_dynamic_repo(repo)
    res = callback.()

    new_state = Map.put(state, :dt, NaiveDateTime.local_now())
    {:reply, res, new_state}
  end

  def handle_info({:EXIT, _from, reason}, state = %__MODULE__{repo: repo}) do
    Supervisor.stop(repo)
    {:stop, reason, state}
  end

  def terminate(_reason, state = %__MODULE__{repo: repo}) do
    Supervisor.stop(repo)
    state
  end
end
