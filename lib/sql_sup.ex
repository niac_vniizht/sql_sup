defmodule SqlSup do
  @moduledoc """
  Documentation for `SqlSup`.
  """
  use Application
  def start(_type, _args), do: SqlSup.Application.start_link([])
end
