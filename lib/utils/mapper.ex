defmodule SqlSup.Mapper do
  @schema_meta_fields [:__meta__]

  def to_storeable_map(struct) when is_struct(struct) do
    association_fields = struct.__struct__.__schema__(:associations)
    waste_fields = association_fields ++ @schema_meta_fields

    struct |> Map.from_struct |> Map.drop(waste_fields)
  end

  def to_storeable_map(struct), do: struct

end
