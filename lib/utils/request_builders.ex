defmodule SqlSup.RequestBuilder do
  @moduledoc """
  Модуль создающий запросы
  """
  require Decimal

  def max_struct(list) do
    list |> Enum.reduce(%{}, fn map, acc ->
      map = SqlSup.Mapper.to_storeable_map(map) |> Enum.filter(& elem(&1, 1)) |> Enum.into(%{})
      Map.merge(acc, map)
    end)
  end

  def only_type_diff(list_1, list_2) do
    d1 = list_1 -- list_2
    d2 = list_2 -- list_1
    cond do
      get_keys(d1) == get_keys(d2) -> :true
      true -> :false
    end
  end

  def get_keys(list), do: list |> Enum.map(& elem(&1, 0))

  def prepare_struct_to_diff(list) do
    list

    |> Enum.map(fn
      %{column_name: name, data_type: type, numeric_precision: numeric_precision, numeric_scale: numeric_scale} -> {name, type, numeric_precision, numeric_scale}
      {key, _val, type} -> {key, type, nil, nil}
    end)
    |> Enum.reject(& elem(&1, 0) == "id")
    |> Enum.map(fn {key, type, numeric_precision, numeric_scale} ->
      cond do
        String.contains?(type, "[]") -> {key, "ARRAY"}
        String.match?(type, ~r/char/) -> {key, "text"}
        String.match?(type, ~r/Array/) -> {key, "ARRAY"}
        String.match?(type, ~r/ARRAY/) -> {key, "ARRAY"}
        String.match?(type, ~r/integer/) -> {key, "bigint"}
        String.match?(type, ~r/int/) -> {key, "bigint"}
        String.match?(type, ~r/numeric/) -> {key, get_numeric_type(numeric_precision, numeric_scale)}
        String.match?(type, ~r/real/) -> {key, "decimal"}
        String.match?(type, ~r/double precision/) -> {key, "decimal"}
        String.match?(type, ~r/timestamp/) -> {key, "timestamp without time zone"}
        true -> {key, type}
      end
    end)
  end

  def get_numeric_type(nil, _scale), do: "numeric"
  def get_numeric_type(numeric_precision, numeric_scale), do: "numeric (#{numeric_precision},#{numeric_scale})"

  def make_maps(%{columns: cols, rows: rows}) when not is_nil(cols) and not is_nil(rows)  do
    at_cols = cols |> Enum.map(&String.to_atom/1)
    rows
    |> Enum.map(&Enum.zip(at_cols, &1) |> Enum.into(%{}))
  end

  def make_maps(_), do: :ok

  @spec fetch_structure(map) :: [any]
  def fetch_structure(struct) when is_struct(struct), do: SqlSup.Mapper.to_storeable_map(struct) |> fetch_structure()

  def fetch_structure(struct) when is_map(struct) do
    struct
    |> Enum.to_list()
    |> Enum.map(fn {key, value} -> {Atom.to_string(key), value, what_type(value)} end)
    |> Enum.reject(& is_nil(elem(&1, 2)))
  end


  @doc """
  Создает запрос на вставку колонок в таблицу

  ## Например

      iex> SqlSup.RequestBuilder.create_add_table_column_query([{"a", "integer"}, {"b", "varchar(255)"}], "test")
      "ALTER TABLE test  ADD COLUMN a integer, ADD COLUMN b varchar(255);"

  """
  @spec create_add_table_column_query(any, any) :: binary()
  def create_add_table_column_query(params, table_name) do
    cols =
      params
      |> Enum.reduce("", & "#{&2} ADD COLUMN IF NOT EXISTS #{elem(&1, 0)} #{elem(&1, 1)},")
      |> String.slice(0..-2)
      |> Kernel.<>(";")

    "ALTER TABLE #{table_name} #{cols}"
  end


  @doc """
  Создает запрос на вставку данных в таблицу

  ## Например

      iex> SqlSup.RequestBuilder.insert_all_basis(["a", "b"], "test")
      "insert into test ( a, b) "

      iex> SqlSup.RequestBuilder.insert_all_basis(["a", "b", "c"], "test")
      "insert into test ( a, b, c)"

  """
  @spec insert_all_basis(any, any) :: binary()
  def insert_all_basis(keys, table_name) do
    keys =
      keys
      |> Enum.reduce("(", & "#{&2} #{&1},")
      |> String.slice(0..-2)
      |> Kernel.<>(")")

    "insert into #{table_name} #{keys} "
  end


  @doc """
  Создает запрос на вставку данных в таблицу

  ## Например

      iex> SqlSup.RequestBuilder.create_insert_query(2, ["a", "b"], "test")
      "insert into test ( a, b) values ( $1, $2)"

      iex> SqlSup.RequestBuilder.create_insert_query(3, ["a", "b", "c"], "test")
      "insert into test ( a, b, c) values ( $1, $2, $3)"

  """
  @spec create_insert_query(integer, any, any) :: binary()
  def create_insert_query(len, keys, table_name) do
    vals =
      1..len
      |> Enum.reduce("(", & "#{&2} $#{&1},")
      |> String.slice(0..-2)
      |> Kernel.<>(")")

    keys =
      keys
      |> Enum.reduce("(", & "#{&2} #{&1},")
      |> String.slice(0..-2)
      |> Kernel.<>(")")

    "insert into #{table_name} #{keys} values #{vals}"
  end


  @doc """
  Создает запрос, который создает таблицу с заданным названием и структурой

  ## Например

      iex> SqlSup.RequestBuilder.create_table_builder([{"a", "mystring", "varchar(250)"}], "test")
      "CREATE TABLE test (id bigserial primary key, a varchar(250) default null);"

      iex> SqlSup.RequestBuilder.create_table_builder([{"a", "mystring", "varchar(250)"}, {"b", 250, "integer"}], "test")
      "CREATE TABLE test (id bigserial primary key, a varchar(250) default null, b integer default null);"

  """
  @spec create_table_builder(maybe_improper_list, any) :: binary()
  def create_table_builder(param_list, table_name) when is_list(param_list) do\
    param_list
    |> Enum.reduce("CREATE TABLE #{table_name} (id bigserial primary key,", fn {key, _value, type}, acc ->
      acc <> " #{key} #{type} default null,"
    end)
    |> String.slice(0..-2)
    |> Kernel.<>(");")
  end

  @doc """
  Определяет тип данных, которые будут использованы при создании таблицы в postgres

  ## Например

      iex> SqlSup.RequestBuilder.what_type("I AM NAME")
      "varchar(255)"

      iex> SqlSup.RequestBuilder.what_type(3)
      "integer"

      iex> SqlSup.RequestBuilder.what_type(35000000000)
      "bigint"

      iex> SqlSup.RequestBuilder.what_type(%{a: 2})
      "json"

      iex> SqlSup.RequestBuilder.what_type(true)
      "boolean"

  """
  @spec what_type(any()) :: nil | binary()
  def what_type(%Date{} = _value), do: "date"
  def what_type(%NaiveDateTime{} = _value), do: "timestamp"
  def what_type(%DateTime{} = _value), do: "timestamp"
  def what_type(value) when is_list(value) do
    val = List.first(value)
    if is_list(val) do
      throw("слишком сложно, зачем такое вообще хранить?")
    else
      "#{what_type(val)}[]"
    end
  end
  def what_type(value) when is_boolean(value), do: "boolean"
  def what_type(value) when is_float(value), do: "decimal"
  def what_type(value) when Decimal.is_decimal(value), do: "decimal"
  def what_type(value) when is_integer(value), do: "bigint"
  def what_type(value) when is_binary(value), do: "text"
  def what_type(value) when is_map(value), do: "json"
  def what_type(_), do: throw("Type does not exist")

  def irreducible_types(type_in_table, type_in_data) do
    case {type_in_table, type_in_data} do
      {"numeric" <> _, "decimal"} -> false
      {"numeric" <> _, "bigint"} -> false
      {"USER-DEFINED", "json"} -> false
      {"decimal", "bigint"} -> false
      {"bigint", "decimal"} -> false
      {"timestamp", "text"} -> false
      {"timestamp without time zone", "text"} -> false
      {"timestamp without time zone", "date"} -> false
      {"boolean", "text"} -> false
      {"ARRAY", "boolean[]"} -> false
      {"ARRAY", "decimal[]"} -> false
      {"ARRAY", "bigint[]"} -> false
      {"date", "text"} -> false
      {"bigint", "text"} -> false
      {_, _} when type_in_table == type_in_data -> false
      {_, _} -> true
    end
  end


end
