defmodule SqlSup.WritterAgent do
  @moduledoc false
  use Agent

  @spec start_link(any) :: {:error, any} | {:ok, pid}
  @spec store_data(list, binary(), list) :: {:ok, non_neg_integer}
  @spec get_item :: %{repo_params: list(), data: list(term()), table: binary()} | nil
  @spec get_item(maybe_improper_list) :: nil | %{repo_params: list(), data: list(term()), table: binary()}

  def start_link(_opts), do: Agent.start_link(fn -> %{} end, name: __MODULE__)

  @doc """
  Enum.to_list(Agent.get(SqlSup.WritterAgent, & &1)) |> Enum.map(fn {{t, _}, data} -> {t, length(data)} end)
  """

  def store_data(repo_params, table, data) do
    key = {table, repo_params}

    Agent.update(__MODULE__, & Map.update(&1, key, data, fn list ->
       Kernel.++(data, list)
    end))

    {:ok, length(data)}
  end

  def get_item, do: get_item(Enum.to_list(Agent.get(__MODULE__, & &1)))
  def get_item([]), do: nil
  def get_item(list) do
    {{table, repo_params}, data} = calc = Enum.max_by(list, & length(elem(&1, 1)))

    Agent.update(__MODULE__, fn _x -> Enum.into(list -- [calc], %{}) end)

    %{
      table: table,
      repo_params: repo_params,
      data: data
    }
  end
end
