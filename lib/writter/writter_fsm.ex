defmodule SqlSup.WritterFsm do
  @moduledoc false
  @name :sql_writter_fsm
  @behaviour :gen_statem
  require Logger

  @operation_timeout 10
  #
  # Client
  #

  def handle(repo_params, table, data) do
    SqlSup.Counter.increment(repo_params[:database], table, :income, length(data), 0)
    :gen_statem.call(:global.whereis_name(:sql_writter_fsm), {:handle, repo_params, table, data})
  end

  #
  # Server
  #
  @doc false
  def start_link(_opts), do: start({:global, @name})

  @doc false
  def start(name) do
    :gen_statem.start_link(name, __MODULE__, 0, [])
  end

  @doc false
  def stop(name), do: :gen_statem.stop(name)

  @doc false
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent
    }
  end

  @doc false
  @impl :gen_statem
  def init(state) do
    {:ok, :inited, state}
  end

  @impl :gen_statem
  @spec callback_mode :: :handle_event_function
  def callback_mode, do: :handle_event_function

  @impl true
  def handle_event({:call, from}, {:handle, repo_params, table, data}, prev, count) do
    SqlSup.WritterAgent.store_data(repo_params, table, data)
    dlen = length(data)
    count = count + dlen

    case prev do
      :inited ->
        {:next_state, :preparing, count,
         [{:state_timeout, @operation_timeout, :fetch_new}, {:reply, from, {:ok, dlen}}]}

      :waiting ->
        {:next_state, :preparing, count,
         [{:state_timeout, @operation_timeout, :fetch_new}, {:reply, from, {:ok, dlen}}]}

      _ ->
        {:keep_state, count, [{:reply, from, {:ok, dlen}}]}
    end
  end

  def handle_event(_any, :fetch_new, :preparing, count) do
    Enum.map(1..2, fn _x -> SqlSup.WritterAgent.get_item() end)
    |> Enum.reject(& is_nil(&1))
    |> case do
      [] -> {:next_state, :waiting, 0, []}
      list -> {:next_state, :sending, count, [{:state_timeout, @operation_timeout, {:send, list}}]}
    end
  end

  @doc """

  for x <- 1..10, do: :global.whereis_name(:sql_writter_fsm)  |> :gen_statem.call({:handle, [database: "14"], "test", [%{a: 3, b: 4}]})
  """
  def handle_event(_any, {:send, list}, :sending, count) do
    Enum.each(list, fn %{table: table, repo_params: repo_params, data: data} ->
      database = repo_params[:database]
      # Logger.warn(inspect([repo_params, data, table]))

      case SqlSup.RepoSup.insert_chunked(repo_params, data, table) do
        {:inserted, written, failed} ->
          if table != "askr_logs",
            do: Logger.debug("Данные успешно записаны в таблицу (#{database}, #{table})")

          SqlSup.Counter.increment(database, table, :insert, written, failed)

        errors when is_list(errors) ->
          Logger.error(
            # "errors: #{errors}, db_name: #{database}, table_name: #{table}, data: #{data}"

            %{
              errors: errors,
              db_name: database,
              table_name: table
            }
          )

        e ->
          Logger.warn(inspect(e))

          Logger.error(%{
            message: "Возникла ошибка при записи данных в таблицу (#{database}, #{table})",
            data: data
          })
      end
    end)


    {:next_state, :preparing, count - Enum.reduce(list, 0, & length(&1.data) + &2),
     [{:state_timeout, @operation_timeout, :fetch_new}]}
  rescue
    e ->
      IO.inspect(e)
      Enum.each(list, fn %{table: table, repo_params: repo_params, data: data} -> SqlSup.WritterAgent.store_data(repo_params, table, data) end)
      {:next_state, :broken, count, [{:state_timeout, @operation_timeout, :fix}]}
  end

  def handle_event(_any, :fix, :broken, count) do
    Process.sleep(:timer.seconds(5))
    {:next_state, :preparing, count, [{:state_timeout, @operation_timeout, :fetch_new}]}
  end

  def handle_event({:call, from}, :empty?, _state, %{stage: nil, unhandled: []} = data),
    do: {:keep_state, data, [{:reply, from, true}]}

  def handle_event({:call, from}, :empty?, _state, data),
    do: {:keep_state, data, [{:reply, from, false}]}

  def handle_event({:call, from}, :check_state, state, data),
    do: {:keep_state, data, [{:reply, from, %{data: data, state: state}}]}

  def handle_event(_any, _event, _state, data), do: {:keep_state, data, []}

  def send_to_watchers([], _), do: :ok
  def send_to_watchers(watchers, msg), do: Enum.each(watchers, &send(&1, msg))
end
