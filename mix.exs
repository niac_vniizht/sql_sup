defmodule SqlSup.MixProject do
  @moduledoc """
  Сервис для работы с SQL запросами
  """
  @version "0.1.1"
  use Mix.Project

  def project do
    [
      app: :sql_sup,
      version: @version,
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
      releases: releases(),
      dialyzer: [
        plt_ignore_apps: [:mnesia, :amnesia]
      ],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        "coveralls.detail": :test,
      ]
    ]
  end

  defp releases do
    [
      sql_sup: [
        include_erts: true,
        path: "sql_sup_release",
        cookie: "8FWrDX6nB9zCreE5",
        include_executables_for: [:unix],
        applications: [
          runtime_tools: :permanent,
          sql_sup: :permanent,
        ]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger, :mnesia, :munin],
      mod: {SqlSup, []}
    ]
  end

  defp deps do
    [
      {:libcluster, "~> 3.2"},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false},
      {:ecto, "~> 3.5"},
      {:ecto_sql, "~> 3.5"},
      {:jason, "~> 1.2"},
      {:postgrex, "~> 0.15.8"},
      {:sentry, "~> 8.0"},
      {:hackney, "~> 1.17"},
      {:geo, "~> 3.0"},
      {:geo_postgis, "~> 3.1"},
      {:poison, "~> 4.0"},
      {:excoveralls, "~> 0.10", only: :test},
      {:munin, git: "http://172.25.78.108/scandinavians/munin.git", tag: "0.1.21"},
      {:r_filer, git: "http://172.25.78.108/scandinavians/r_filer.git", tag: "v0.1.3"},
      {:timex, "~> 3.5"},
    ]
  end

  defp aliases do
    [
      test: "test"
    ]
  end
end
