defmodule SqlSup.RepoSupTest do
  use ExUnit.Case
  alias SqlSup.RepoSup
  import Ecto.Query
  doctest SqlSup.RepoSup

  @params2 [
    hostname: "localhost",
    username: "postgres",
    password: "postgres",
    database: "simple_test",
    port: 5432
  ]

  @params1 [
    hostname: "localhost",
    username: "postgres",
    password: "postgres",
    database: "postgres",
    port: 5432
  ]

  # test "Еим базы в кашу миллионом запросов" do
  #   q = fn params -> SqlSup.RepoSup.run_query(params, fn -> SqlSup.Repo.all(from t in "test", select: t.id) end) end

  #   import Ecto.Query
  #   # :observer.start()
  #   # SqlSup.RepoSup.run_query([hostname: "localhost", username: "postgres", password: "postgres",  database: "simple_test", port: 5432 ], fn -> SqlSup.Repo.all(from t in "test", select: t.id) end)
  #   # SqlSup.RepoSup.run_query([hostname: "localhost", username: "postgres", password: "postgres",  database: "postgres", port: 5432 ], fn -> SqlSup.Repo.all(from t in "test", select: t.id) end)

  #   # SqlSup.RepoSup.run_query([hostname: "localhost", username: "postgres", password: "postgres",  database: "simple_test", port: 5432 ], fn -> SqlSup.Repo.insert_all("test", [%{a: 1, b: "str"}, %{a: 3, b: hey}]) end)

  #   res =
  #     1..20_000
  #     |> Stream.each(fn _x -> q.(@params1) end)
  #     |> Stream.each(fn _x -> q.(@params2) end)
  #     |> Enum.to_list()

  #   len = length(res)

  #   assert true = is_list(res)
  #   assert 20_000 = len
  # end

  describe "Тест запросов" do
    setup do
      %{
        repo_params: [
          hostname: "172.25.78.167",
          username: "db_admin",
          password: "8FWrDX6nB9zCreE5",
          database: "askr_platform",
          port: 5437
        ],
        table_name: "test_table",
        inserted_list: [%{a: 1, b: "test", fee: 477666.3688888888888888888}]
      }
    end

    test "тест добавления данных", %{
      repo_params: repo_params,
      table_name: table_name,
      inserted_list: inserted_list
    } do
      assert RepoSup.insert_chunked(repo_params, inserted_list, table_name) == 1
    end

    test "тест получения структуры таблицы", %{repo_params: repo_params, table_name: table_name} do
      assert RepoSup.get_table_struct(repo_params, table_name) == [
               %{column_name: "id", data_type: "bigint"},
               %{column_name: "a", data_type: "bigint"},
               %{column_name: "b", data_type: "text"}
             ]
    end
  end
end
