defmodule SqlSup.RequestBuilderTest do
  use ExUnit.Case
  alias SqlSup.RepoSup
  alias SqlSup.RequestBuilder
  import Ecto.Query

  describe "Тест создания запросов" do
    setup do
      %{
        hostname: "172.25.78.167",
        username: "db_admin",
        password: "8FWrDX6nB9zCreE5",
        database: "simple_data",
        port: 5437,
        table_name: "test_table"
      }
    end

    test "тест создания нового столбца", %{table_name: table_name} do
      assert RequestBuilder.create_add_table_column_query(
               [{"a", "integer"}, {"b", "varchar(255)"}],
               table_name
             ) ==
               "ALTER TABLE #{table_name}  ADD COLUMN IF NOT EXISTS a integer, ADD COLUMN IF NOT EXISTS b varchar(255);"
    end

    test "тест проверки наличия у двух списков элементов с одним названием и разными типами" do
      list_1 = [{"a", "bigint"}, {"b", "text"}]
      list_2 = [{"a", "bigint"}, {"b", "bigint"}, {"c", "text"}]
    end

    test "тест сравнения типов" do
      assert RequestBuilder.irreducible_types("int", "int") == false
      assert RequestBuilder.irreducible_types("decimal", "real") == true
      assert RequestBuilder.irreducible_types("decimal", "int") == true
    end
  end
end
