defmodule SqlSup.WritterFsmTest do
  use ExUnit.Case



  describe "Тест конечного автомата" do
    setup do
      %{
        repo_params: [
          hostname: "172.25.78.167",
          username: "db_admin",
          password: "8FWrDX6nB9zCreE5",
          database: "askr_platform",
          port: 5437
        ],
        table_name: "test_table",
        inserted_list: [%{a: 1, b: "test", fee: 4000.3688888888888888888}]
      }
    end

    test "тест запроса", %{
      repo_params: repo_params,
      table_name: table_name,
      inserted_list: inserted_list
    } do
      assert SqlSup.WritterFsm.handle(repo_params, table_name, inserted_list) == {:ok, 1}
    end
  end
end
